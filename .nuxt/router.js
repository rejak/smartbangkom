import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _4a3c863c = () => interopDefault(import('..\\pages\\Content.vue' /* webpackChunkName: "pages/Content" */))
const _67b2f112 = () => interopDefault(import('..\\pages\\ContentFooter.vue' /* webpackChunkName: "pages/ContentFooter" */))
const _b35f6f92 = () => interopDefault(import('..\\pages\\dashboard.vue' /* webpackChunkName: "pages/dashboard" */))
const _4801b06b = () => interopDefault(import('..\\pages\\dashboard\\index.vue' /* webpackChunkName: "pages/dashboard/index" */))
const _7f49f8ad = () => interopDefault(import('..\\pages\\dashboard\\backup dashboard.vue' /* webpackChunkName: "pages/dashboard/backup dashboard" */))
const _459a3382 = () => interopDefault(import('..\\pages\\dashboard\\check_dokumen\\index.vue' /* webpackChunkName: "pages/dashboard/check_dokumen/index" */))
const _bb662110 = () => interopDefault(import('..\\pages\\dashboard\\DetailPr\\index.vue' /* webpackChunkName: "pages/dashboard/DetailPr/index" */))
const _50cadad2 = () => interopDefault(import('..\\pages\\dashboard\\Diklat\\index.vue' /* webpackChunkName: "pages/dashboard/Diklat/index" */))
const _2e95d3d2 = () => interopDefault(import('..\\pages\\dashboard\\id.vue' /* webpackChunkName: "pages/dashboard/id" */))
const _7ee6e56f = () => interopDefault(import('..\\pages\\dashboard\\kra\\index.vue' /* webpackChunkName: "pages/dashboard/kra/index" */))
const _b4e7f83e = () => interopDefault(import('..\\pages\\dashboard\\list_esertifikat\\index.vue' /* webpackChunkName: "pages/dashboard/list_esertifikat/index" */))
const _8672ae6c = () => interopDefault(import('..\\pages\\dashboard\\ListLemdikAdmin\\index.vue' /* webpackChunkName: "pages/dashboard/ListLemdikAdmin/index" */))
const _08b3de56 = () => interopDefault(import('..\\pages\\dashboard\\manajemen_user\\index.vue' /* webpackChunkName: "pages/dashboard/manajemen_user/index" */))
const _55440c87 = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran\\index.vue' /* webpackChunkName: "pages/dashboard/pendaftaran/index" */))
const _59ef0d49 = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran_ttd_digital\\index.vue' /* webpackChunkName: "pages/dashboard/pendaftaran_ttd_digital/index" */))
const _7513b203 = () => interopDefault(import('..\\pages\\dashboard\\penerbitanKRA\\index.vue' /* webpackChunkName: "pages/dashboard/penerbitanKRA/index" */))
const _c51198f6 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\index.vue' /* webpackChunkName: "pages/dashboard/perencanaan/index" */))
const _6ee3c82f = () => interopDefault(import('..\\pages\\dashboard\\profil\\index.vue' /* webpackChunkName: "pages/dashboard/profil/index" */))
const _a5cdb5b0 = () => interopDefault(import('..\\pages\\dashboard\\register\\index.vue' /* webpackChunkName: "pages/dashboard/register/index" */))
const _7522a5ea = () => interopDefault(import('..\\pages\\dashboard\\rencana_penyelenggaraaan\\index.vue' /* webpackChunkName: "pages/dashboard/rencana_penyelenggaraaan/index" */))
const _daa96d52 = () => interopDefault(import('..\\pages\\dashboard\\sertifikatDigital\\index.vue' /* webpackChunkName: "pages/dashboard/sertifikatDigital/index" */))
const _77eaee10 = () => interopDefault(import('..\\pages\\dashboard\\tandatangan\\index.vue' /* webpackChunkName: "pages/dashboard/tandatangan/index" */))
const _5d1d80c9 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\index.vue' /* webpackChunkName: "pages/dashboard/terbitizin/index" */))
const _5aa35f60 = () => interopDefault(import('..\\pages\\dashboard\\User\\index.vue' /* webpackChunkName: "pages/dashboard/User/index" */))
const _3ac37633 = () => interopDefault(import('..\\pages\\dashboard\\verifpeserta\\index.vue' /* webpackChunkName: "pages/dashboard/verifpeserta/index" */))
const _2b513658 = () => interopDefault(import('..\\pages\\dashboard\\DetailPr\\edit.vue' /* webpackChunkName: "pages/dashboard/DetailPr/edit" */))
const _d5bcb38c = () => interopDefault(import('..\\pages\\dashboard\\Diklat\\edit.vue' /* webpackChunkName: "pages/dashboard/Diklat/edit" */))
const _403faade = () => interopDefault(import('..\\pages\\dashboard\\Diklat\\info.vue' /* webpackChunkName: "pages/dashboard/Diklat/info" */))
const _b77079ce = () => interopDefault(import('..\\pages\\dashboard\\Diklat\\tambah.vue' /* webpackChunkName: "pages/dashboard/Diklat/tambah" */))
const _136e4ff8 = () => interopDefault(import('..\\pages\\dashboard\\kra\\detail.vue' /* webpackChunkName: "pages/dashboard/kra/detail" */))
const _3a32c74d = () => interopDefault(import('..\\pages\\dashboard\\kra\\detaillama.vue' /* webpackChunkName: "pages/dashboard/kra/detaillama" */))
const _635a230f = () => interopDefault(import('..\\pages\\dashboard\\kra\\downloadKRA.vue' /* webpackChunkName: "pages/dashboard/kra/downloadKRA" */))
const _43726ee2 = () => interopDefault(import('..\\pages\\dashboard\\ListLemdikAdmin\\ListDetailPelatihanAdmin\\index.vue' /* webpackChunkName: "pages/dashboard/ListLemdikAdmin/ListDetailPelatihanAdmin/index" */))
const _7fa4793d = () => interopDefault(import('..\\pages\\dashboard\\master\\golpang\\index.vue' /* webpackChunkName: "pages/dashboard/master/golpang/index" */))
const _091e6be0 = () => interopDefault(import('..\\pages\\dashboard\\master\\instansi\\index.vue' /* webpackChunkName: "pages/dashboard/master/instansi/index" */))
const _f551ac70 = () => interopDefault(import('..\\pages\\dashboard\\master\\kabupaten\\index.vue' /* webpackChunkName: "pages/dashboard/master/kabupaten/index" */))
const _7ec27897 = () => interopDefault(import('..\\pages\\dashboard\\master\\lemdik\\index.vue' /* webpackChunkName: "pages/dashboard/master/lemdik/index" */))
const _0e63204d = () => interopDefault(import('..\\pages\\dashboard\\master\\masterTemplate\\index.vue' /* webpackChunkName: "pages/dashboard/master/masterTemplate/index" */))
const _2ceded20 = () => interopDefault(import('..\\pages\\dashboard\\master\\prodik\\index.vue' /* webpackChunkName: "pages/dashboard/master/prodik/index" */))
const _0a663665 = () => interopDefault(import('..\\pages\\dashboard\\master\\provinsi\\index.vue' /* webpackChunkName: "pages/dashboard/master/provinsi/index" */))
const _d376680a = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran\\Formdaftar.vue' /* webpackChunkName: "pages/dashboard/pendaftaran/Formdaftar" */))
const _16e2f755 = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran\\listDaftar.vue' /* webpackChunkName: "pages/dashboard/pendaftaran/listDaftar" */))
const _16562a92 = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran\\pendaftaran.vue' /* webpackChunkName: "pages/dashboard/pendaftaran/pendaftaran" */))
const _4f4fed32 = () => interopDefault(import('..\\pages\\dashboard\\pendaftaran\\users.js' /* webpackChunkName: "pages/dashboard/pendaftaran/users" */))
const _ac7b66a4 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\detail.vue' /* webpackChunkName: "pages/dashboard/perencanaan/detail" */))
const _23fdeb27 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\edit.vue' /* webpackChunkName: "pages/dashboard/perencanaan/edit" */))
const _1c51849e = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\inforencana\\index.vue' /* webpackChunkName: "pages/dashboard/perencanaan/inforencana/index" */))
const _25b2fe74 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\tambah.vue' /* webpackChunkName: "pages/dashboard/perencanaan/tambah" */))
const _4100e17a = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\verifpermohonan\\index.vue' /* webpackChunkName: "pages/dashboard/perencanaan/verifpermohonan/index" */))
const _6d9ecf92 = () => interopDefault(import('..\\pages\\dashboard\\profil\\index1.vue' /* webpackChunkName: "pages/dashboard/profil/index1" */))
const _f9488e18 = () => interopDefault(import('..\\pages\\dashboard\\register\\peserta\\index.vue' /* webpackChunkName: "pages/dashboard/register/peserta/index" */))
const _514d538e = () => interopDefault(import('..\\pages\\dashboard\\rencana_penyelenggaraaan\\add.vue' /* webpackChunkName: "pages/dashboard/rencana_penyelenggaraaan/add" */))
const _051bf042 = () => interopDefault(import('..\\pages\\dashboard\\sertifikatDigital\\detailPengajuan.vue' /* webpackChunkName: "pages/dashboard/sertifikatDigital/detailPengajuan" */))
const _0ea02b20 = () => interopDefault(import('..\\pages\\dashboard\\sertifikatDigital\\pengajuan.vue' /* webpackChunkName: "pages/dashboard/sertifikatDigital/pengajuan" */))
const _21244cb6 = () => interopDefault(import('..\\pages\\dashboard\\sertifikatDigital\\peserta\\index.vue' /* webpackChunkName: "pages/dashboard/sertifikatDigital/peserta/index" */))
const _2a82c7fa = () => interopDefault(import('..\\pages\\dashboard\\tandatangan\\certpeserta.vue' /* webpackChunkName: "pages/dashboard/tandatangan/certpeserta" */))
const _5599a59b = () => interopDefault(import('..\\pages\\dashboard\\tandatangan\\detail.vue' /* webpackChunkName: "pages/dashboard/tandatangan/detail" */))
const _31d0062a = () => interopDefault(import('..\\pages\\dashboard\\tandatangan\\gambar.vue' /* webpackChunkName: "pages/dashboard/tandatangan/gambar" */))
const _763e4803 = () => interopDefault(import('..\\pages\\dashboard\\tandatangan\\revisiSttp.vue' /* webpackChunkName: "pages/dashboard/tandatangan/revisiSttp" */))
const _4234b02c = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\detail.vue' /* webpackChunkName: "pages/dashboard/terbitizin/detail" */))
const _83b762f4 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\otorisasi\\index.vue' /* webpackChunkName: "pages/dashboard/terbitizin/otorisasi/index" */))
const _b9f12374 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\refpelatihan\\index.vue' /* webpackChunkName: "pages/dashboard/terbitizin/refpelatihan/index" */))
const _691ca973 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\verifpelatihan\\index.vue' /* webpackChunkName: "pages/dashboard/terbitizin/verifpelatihan/index" */))
const _3b40ec7e = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\view.vue' /* webpackChunkName: "pages/dashboard/terbitizin/view" */))
const _70e46449 = () => interopDefault(import('..\\pages\\dashboard\\verifpeserta\\peserta\\index.vue' /* webpackChunkName: "pages/dashboard/verifpeserta/peserta/index" */))
const _79a3061e = () => interopDefault(import('..\\pages\\dashboard\\ListLemdikAdmin\\ListDetailPelatihanAdmin\\detail.vue' /* webpackChunkName: "pages/dashboard/ListLemdikAdmin/ListDetailPelatihanAdmin/detail" */))
const _43a9e548 = () => interopDefault(import('..\\pages\\dashboard\\master\\masterTemplate\\add.vue' /* webpackChunkName: "pages/dashboard/master/masterTemplate/add" */))
const _0573ad80 = () => interopDefault(import('..\\pages\\dashboard\\master\\masterTemplate\\delete.vue' /* webpackChunkName: "pages/dashboard/master/masterTemplate/delete" */))
const _da057542 = () => interopDefault(import('..\\pages\\dashboard\\master\\masterTemplate\\edit.vue' /* webpackChunkName: "pages/dashboard/master/masterTemplate/edit" */))
const _5532471d = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\inforencana\\ajukan.vue' /* webpackChunkName: "pages/dashboard/perencanaan/inforencana/ajukan" */))
const _bad212a0 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\inforencana\\infodetail.vue' /* webpackChunkName: "pages/dashboard/perencanaan/inforencana/infodetail" */))
const _7dacb774 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\inforencana\\infopermohonan.vue' /* webpackChunkName: "pages/dashboard/perencanaan/inforencana/infopermohonan" */))
const _083c2a92 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\verifpermohonan\\edit.vue' /* webpackChunkName: "pages/dashboard/perencanaan/verifpermohonan/edit" */))
const _994ba194 = () => interopDefault(import('..\\pages\\dashboard\\perencanaan\\verifpermohonan\\info.vue' /* webpackChunkName: "pages/dashboard/perencanaan/verifpermohonan/info" */))
const _267edf38 = () => interopDefault(import('..\\pages\\dashboard\\profil\\UserProfile\\EditProfileForm.vue' /* webpackChunkName: "pages/dashboard/profil/UserProfile/EditProfileForm" */))
const _e4b2b694 = () => interopDefault(import('..\\pages\\dashboard\\profil\\UserProfile\\UserCard.vue' /* webpackChunkName: "pages/dashboard/profil/UserProfile/UserCard" */))
const _b7c1bfd6 = () => interopDefault(import('..\\pages\\dashboard\\register\\peserta\\tambahasn.vue' /* webpackChunkName: "pages/dashboard/register/peserta/tambahasn" */))
const _c5f651f4 = () => interopDefault(import('..\\pages\\dashboard\\register\\peserta\\tambahnonasn.vue' /* webpackChunkName: "pages/dashboard/register/peserta/tambahnonasn" */))
const _78ebfdf2 = () => interopDefault(import('..\\pages\\dashboard\\sertifikatDigital\\peserta\\edit.vue' /* webpackChunkName: "pages/dashboard/sertifikatDigital/peserta/edit" */))
const _c28edc66 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\otorisasi\\detail.vue' /* webpackChunkName: "pages/dashboard/terbitizin/otorisasi/detail" */))
const _56396a0d = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\refpelatihan\\detail.vue' /* webpackChunkName: "pages/dashboard/terbitizin/refpelatihan/detail" */))
const _f056493e = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\refpelatihan\\view.vue' /* webpackChunkName: "pages/dashboard/terbitizin/refpelatihan/view" */))
const _52cb9480 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\verifpelatihan\\detail.vue' /* webpackChunkName: "pages/dashboard/terbitizin/verifpelatihan/detail" */))
const _8075f2d8 = () => interopDefault(import('..\\pages\\dashboard\\terbitizin\\verifpelatihan\\view.vue' /* webpackChunkName: "pages/dashboard/terbitizin/verifpelatihan/view" */))
const _6301aadb = () => interopDefault(import('..\\pages\\dashboard\\verifpeserta\\peserta\\detailangkatan.vue' /* webpackChunkName: "pages/dashboard/verifpeserta/peserta/detailangkatan" */))
const _3070e5f2 = () => interopDefault(import('..\\pages\\DashboardNavbar.vue' /* webpackChunkName: "pages/DashboardNavbar" */))
const _4e55180c = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _f6a51616 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/Content",
    component: _4a3c863c,
    name: "Content"
  }, {
    path: "/ContentFooter",
    component: _67b2f112,
    name: "ContentFooter"
  }, {
    path: "/dashboard",
    component: _b35f6f92,
    children: [{
      path: "",
      component: _4801b06b,
      name: "dashboard"
    }, {
      path: "backup dashboard",
      component: _7f49f8ad,
      name: "dashboard-backup dashboard"
    }, {
      path: "check_dokumen",
      component: _459a3382,
      name: "dashboard-check_dokumen"
    }, {
      path: "DetailPr",
      component: _bb662110,
      name: "dashboard-DetailPr"
    }, {
      path: "Diklat",
      component: _50cadad2,
      name: "dashboard-Diklat"
    }, {
      path: "id",
      component: _2e95d3d2,
      name: "dashboard-id"
    }, {
      path: "kra",
      component: _7ee6e56f,
      name: "dashboard-kra"
    }, {
      path: "list_esertifikat",
      component: _b4e7f83e,
      name: "dashboard-list_esertifikat"
    }, {
      path: "ListLemdikAdmin",
      component: _8672ae6c,
      name: "dashboard-ListLemdikAdmin"
    }, {
      path: "manajemen_user",
      component: _08b3de56,
      name: "dashboard-manajemen_user"
    }, {
      path: "pendaftaran",
      component: _55440c87,
      name: "dashboard-pendaftaran"
    }, {
      path: "pendaftaran_ttd_digital",
      component: _59ef0d49,
      name: "dashboard-pendaftaran_ttd_digital"
    }, {
      path: "penerbitanKRA",
      component: _7513b203,
      name: "dashboard-penerbitanKRA"
    }, {
      path: "perencanaan",
      component: _c51198f6,
      name: "dashboard-perencanaan"
    }, {
      path: "profil",
      component: _6ee3c82f,
      name: "dashboard-profil"
    }, {
      path: "register",
      component: _a5cdb5b0,
      name: "dashboard-register"
    }, {
      path: "rencana_penyelenggaraaan",
      component: _7522a5ea,
      name: "dashboard-rencana_penyelenggaraaan"
    }, {
      path: "sertifikatDigital",
      component: _daa96d52,
      name: "dashboard-sertifikatDigital"
    }, {
      path: "tandatangan",
      component: _77eaee10,
      name: "dashboard-tandatangan"
    }, {
      path: "terbitizin",
      component: _5d1d80c9,
      name: "dashboard-terbitizin"
    }, {
      path: "User",
      component: _5aa35f60,
      name: "dashboard-User"
    }, {
      path: "verifpeserta",
      component: _3ac37633,
      name: "dashboard-verifpeserta"
    }, {
      path: "DetailPr/edit",
      component: _2b513658,
      name: "dashboard-DetailPr-edit"
    }, {
      path: "Diklat/edit",
      component: _d5bcb38c,
      name: "dashboard-Diklat-edit"
    }, {
      path: "Diklat/info",
      component: _403faade,
      name: "dashboard-Diklat-info"
    }, {
      path: "Diklat/tambah",
      component: _b77079ce,
      name: "dashboard-Diklat-tambah"
    }, {
      path: "kra/detail",
      component: _136e4ff8,
      name: "dashboard-kra-detail"
    }, {
      path: "kra/detaillama",
      component: _3a32c74d,
      name: "dashboard-kra-detaillama"
    }, {
      path: "kra/downloadKRA",
      component: _635a230f,
      name: "dashboard-kra-downloadKRA"
    }, {
      path: "ListLemdikAdmin/ListDetailPelatihanAdmin",
      component: _43726ee2,
      name: "dashboard-ListLemdikAdmin-ListDetailPelatihanAdmin"
    }, {
      path: "master/golpang",
      component: _7fa4793d,
      name: "dashboard-master-golpang"
    }, {
      path: "master/instansi",
      component: _091e6be0,
      name: "dashboard-master-instansi"
    }, {
      path: "master/kabupaten",
      component: _f551ac70,
      name: "dashboard-master-kabupaten"
    }, {
      path: "master/lemdik",
      component: _7ec27897,
      name: "dashboard-master-lemdik"
    }, {
      path: "master/masterTemplate",
      component: _0e63204d,
      name: "dashboard-master-masterTemplate"
    }, {
      path: "master/prodik",
      component: _2ceded20,
      name: "dashboard-master-prodik"
    }, {
      path: "master/provinsi",
      component: _0a663665,
      name: "dashboard-master-provinsi"
    }, {
      path: "pendaftaran/Formdaftar",
      component: _d376680a,
      name: "dashboard-pendaftaran-Formdaftar"
    }, {
      path: "pendaftaran/listDaftar",
      component: _16e2f755,
      name: "dashboard-pendaftaran-listDaftar"
    }, {
      path: "pendaftaran/pendaftaran",
      component: _16562a92,
      name: "dashboard-pendaftaran-pendaftaran"
    }, {
      path: "pendaftaran/users",
      component: _4f4fed32,
      name: "dashboard-pendaftaran-users"
    }, {
      path: "perencanaan/detail",
      component: _ac7b66a4,
      name: "dashboard-perencanaan-detail"
    }, {
      path: "perencanaan/edit",
      component: _23fdeb27,
      name: "dashboard-perencanaan-edit"
    }, {
      path: "perencanaan/inforencana",
      component: _1c51849e,
      name: "dashboard-perencanaan-inforencana"
    }, {
      path: "perencanaan/tambah",
      component: _25b2fe74,
      name: "dashboard-perencanaan-tambah"
    }, {
      path: "perencanaan/verifpermohonan",
      component: _4100e17a,
      name: "dashboard-perencanaan-verifpermohonan"
    }, {
      path: "profil/index1",
      component: _6d9ecf92,
      name: "dashboard-profil-index1"
    }, {
      path: "register/peserta",
      component: _f9488e18,
      name: "dashboard-register-peserta"
    }, {
      path: "rencana_penyelenggaraaan/add",
      component: _514d538e,
      name: "dashboard-rencana_penyelenggaraaan-add"
    }, {
      path: "sertifikatDigital/detailPengajuan",
      component: _051bf042,
      name: "dashboard-sertifikatDigital-detailPengajuan"
    }, {
      path: "sertifikatDigital/pengajuan",
      component: _0ea02b20,
      name: "dashboard-sertifikatDigital-pengajuan"
    }, {
      path: "sertifikatDigital/peserta",
      component: _21244cb6,
      name: "dashboard-sertifikatDigital-peserta"
    }, {
      path: "tandatangan/certpeserta",
      component: _2a82c7fa,
      name: "dashboard-tandatangan-certpeserta"
    }, {
      path: "tandatangan/detail",
      component: _5599a59b,
      name: "dashboard-tandatangan-detail"
    }, {
      path: "tandatangan/gambar",
      component: _31d0062a,
      name: "dashboard-tandatangan-gambar"
    }, {
      path: "tandatangan/revisiSttp",
      component: _763e4803,
      name: "dashboard-tandatangan-revisiSttp"
    }, {
      path: "terbitizin/detail",
      component: _4234b02c,
      name: "dashboard-terbitizin-detail"
    }, {
      path: "terbitizin/otorisasi",
      component: _83b762f4,
      name: "dashboard-terbitizin-otorisasi"
    }, {
      path: "terbitizin/refpelatihan",
      component: _b9f12374,
      name: "dashboard-terbitizin-refpelatihan"
    }, {
      path: "terbitizin/verifpelatihan",
      component: _691ca973,
      name: "dashboard-terbitizin-verifpelatihan"
    }, {
      path: "terbitizin/view",
      component: _3b40ec7e,
      name: "dashboard-terbitizin-view"
    }, {
      path: "verifpeserta/peserta",
      component: _70e46449,
      name: "dashboard-verifpeserta-peserta"
    }, {
      path: "ListLemdikAdmin/ListDetailPelatihanAdmin/detail",
      component: _79a3061e,
      name: "dashboard-ListLemdikAdmin-ListDetailPelatihanAdmin-detail"
    }, {
      path: "master/masterTemplate/add",
      component: _43a9e548,
      name: "dashboard-master-masterTemplate-add"
    }, {
      path: "master/masterTemplate/delete",
      component: _0573ad80,
      name: "dashboard-master-masterTemplate-delete"
    }, {
      path: "master/masterTemplate/edit",
      component: _da057542,
      name: "dashboard-master-masterTemplate-edit"
    }, {
      path: "perencanaan/inforencana/ajukan",
      component: _5532471d,
      name: "dashboard-perencanaan-inforencana-ajukan"
    }, {
      path: "perencanaan/inforencana/infodetail",
      component: _bad212a0,
      name: "dashboard-perencanaan-inforencana-infodetail"
    }, {
      path: "perencanaan/inforencana/infopermohonan",
      component: _7dacb774,
      name: "dashboard-perencanaan-inforencana-infopermohonan"
    }, {
      path: "perencanaan/verifpermohonan/edit",
      component: _083c2a92,
      name: "dashboard-perencanaan-verifpermohonan-edit"
    }, {
      path: "perencanaan/verifpermohonan/info",
      component: _994ba194,
      name: "dashboard-perencanaan-verifpermohonan-info"
    }, {
      path: "profil/UserProfile/EditProfileForm",
      component: _267edf38,
      name: "dashboard-profil-UserProfile-EditProfileForm"
    }, {
      path: "profil/UserProfile/UserCard",
      component: _e4b2b694,
      name: "dashboard-profil-UserProfile-UserCard"
    }, {
      path: "register/peserta/tambahasn",
      component: _b7c1bfd6,
      name: "dashboard-register-peserta-tambahasn"
    }, {
      path: "register/peserta/tambahnonasn",
      component: _c5f651f4,
      name: "dashboard-register-peserta-tambahnonasn"
    }, {
      path: "sertifikatDigital/peserta/edit",
      component: _78ebfdf2,
      name: "dashboard-sertifikatDigital-peserta-edit"
    }, {
      path: "terbitizin/otorisasi/detail",
      component: _c28edc66,
      name: "dashboard-terbitizin-otorisasi-detail"
    }, {
      path: "terbitizin/refpelatihan/detail",
      component: _56396a0d,
      name: "dashboard-terbitizin-refpelatihan-detail"
    }, {
      path: "terbitizin/refpelatihan/view",
      component: _f056493e,
      name: "dashboard-terbitizin-refpelatihan-view"
    }, {
      path: "terbitizin/verifpelatihan/detail",
      component: _52cb9480,
      name: "dashboard-terbitizin-verifpelatihan-detail"
    }, {
      path: "terbitizin/verifpelatihan/view",
      component: _8075f2d8,
      name: "dashboard-terbitizin-verifpelatihan-view"
    }, {
      path: "verifpeserta/peserta/detailangkatan",
      component: _6301aadb,
      name: "dashboard-verifpeserta-peserta-detailangkatan"
    }]
  }, {
    path: "/DashboardNavbar",
    component: _3070e5f2,
    name: "DashboardNavbar"
  }, {
    path: "/login",
    component: _4e55180c,
    name: "login"
  }, {
    path: "/",
    component: _f6a51616,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
