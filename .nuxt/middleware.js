const middleware = {}

middleware['auth'] = require('..\\middleware\\auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['ceklogin'] = require('..\\middleware\\ceklogin.js')
middleware['ceklogin'] = middleware['ceklogin'].default || middleware['ceklogin']

middleware['login'] = require('..\\middleware\\login.js')
middleware['login'] = middleware['login'].default || middleware['login']

export default middleware
