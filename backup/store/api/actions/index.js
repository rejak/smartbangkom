import {Action,Mutation} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';


import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

var urldiklat="https://smartbangkom.lan.go.id:8091/";
var urlpeserta="https://smartbangkom.lan.go.id:8092/";
var urlperizinan="https://smartbangkom.lan.go.id:8093/";
var urluser ="https://smartbangkom.lan.go.id:8090/";
var urlmaster="https://sipka.lan.go.id/api/";

export default {

    //api diklat
    //upload
    [Action.diklat.uploadkra]({},isi){

        var formdata = new FormData()
        formdata.append('permintaankra',isi.permintaan)
        formdata.append('daftarhadir',isi.daftarhadir)

        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/uploaddokumen/'+isi.iddiklat,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    //end upload
    [Action.diklat.downloaddoc]({},nama){

        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/downloaddockra/'+nama+'.',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.penandatangan]({},isi){
        return new Promise((resolve,reject)=>{

            Axios.put(urldiklat+'diklat/penandatangan/'+isi.iddiklat+"/"+isi.ttd1+"/"+isi.ttd2+"/"+isi.ttd3,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilsemuadiklat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuserid]({},iduser){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byiduser/'+iduser,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpandiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/simpan',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.kirimdiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/kirim',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.hitungpeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/countpeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildetaildiklat]({},id){
        // //(id);

        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpandetail]({},isi){
        //data isi {jumpes:0,asal:"",sumber:"",pola:""}
        return new Promise((resolve,reject)=>{
            let data={
                "jumpes":isi.jumpes,
                "asalInstansi":isi.asal,
                "sumberAnggaran":isi.sumber,
                "polaPenyelenggaraan":isi.pola
            };
            Axios.post(urldiklat+'diklat/detail/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ubahtanggalttd]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            Axios.put(urldiklat+'diklat/tglttd/'+isi.iddiklat+'/'+isi.tgl,'', {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    [Action.diklat.editdetail]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up = isi[n];
                Axios.put(urldiklat+'diklat/editDetail/'+element_up.id, element_up, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        })
    },
    [Action.diklat.deletedetail]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.ambilangkatan]({},id){
        // //(id);

        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpanangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let data={
                "alamat":isi.alamat,
	            "daerah":isi.daerah
            };
            Axios.post(urldiklat+'diklat/angkatan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.editangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up_angkatan = isi[n];
                Axios.put(urldiklat+'diklat/angkatan/'+element_up_angkatan.id, element_up_angkatan, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        })
    },
    [Action.diklat.delelteangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end diklat

    //api kra
    [Action.cert.datagbr]({},namagbr){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/gambarttd/'+namagbr+'.','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.gbtttd]({},isi){
        var formdata = new FormData()
        formdata.append('gambar',isi.base64)
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/simpangbrttd/'+isi.iduser,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.ttdcert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddigital/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(function (error) {
                reject(error)
                if (error.response) {
                // commit(`${[Mutation.error]}`,error.response.status)
                }
            }

            )
        })
    },
    [Action.cert.ttdcertkalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdkalandev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.ttdcertdeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddeputidev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    //riil
    [Action.cert.cekganti]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ubahpdf/'+isi.idpeserta,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandacert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdlemdik/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandakalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdkalan/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandadeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddeputi/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //endriil
    [Action.cert.generate]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/gencert/'+isi.id,{
                "jp":isi.jp,
                "penyelenggara":isi.p,
                "status":isi.sts,
                "pm":isi.pm,
                "pminstansi":isi.pminstansi,
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.kra.ambil](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'kra',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.kra.edit]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'kra/'+isi.id,{
                "jenisprogram":isi.jenis,
                "nomor":isi.nomor,
                "tahun":isi.tahun
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api kra

    //api peserta
    [Action.peserta.tambahkra]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahkra/'+isi.idpeserta,{
                "nomorKra":isi.kra
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.tambahnilai]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahnilai/'+isi.idpeserta,{
                "proyekPerubahan":isi.proyek,
                "nilai":isi.nilai
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.peserta.updatePeserta]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/updatekuota/'+isi.iddiklat+'/'+isi.kuota,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilbkn]({},nip){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'bkn/datapegawai/'+nip,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpesertakelompok]({},isi){
        //data isi {instansi:"",alamat:"",dokumen:"",jeniskerja:"",id:0}
        let form={
            "asalInstansi":isi.instansi,
            "alamatKantor":isi.alamat,
            "jenisPekerjaan":isi.jeniskerja,
        }
        var formdata = new FormData()
        formdata.append('excel',isi.dokumen)
        formdata.append('isi',JSON.stringify(form))

        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelompok/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabydiklat]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{
            let data={
                    "jabatan":isi.jabatan,
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol
            };
            Axios.post(urlpeserta+'peserta',data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.editpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{

            let data={
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol
            };
            Axios.put(urlpeserta+'peserta/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.editfotopeserta]({},isi){
        //isi data {id:0,foto:filefoto}
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/'+isi.id,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.deletepeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahangkatan]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelaspeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahdiklat]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/pindahpeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.downloadsampel]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/download/'+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api peserta

    //api izin
    [Action.izin.ambilizin](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ambilizinbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/izinbyuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.editizin]({},isi){
        //data isi {id:0,iddiklat:0,penjaminmutu:"",izindeputi:""}
        return new Promise((resolve,reject)=>{
            let data={
                "idDiklat": isi.iddiklat,
                "penjaminMutu": isi.penjaminmutu,
                "izinDeputi": isi.izindeputi,
            }
            Axios.put(urlperizinan+'perizinan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.deleteizin]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlperizinan+'perizinan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ajukanizin]({},isi){

        //data isi {id:0,penjaminmutu:"",dokumen:filedokumen}
        let data ={
            "penjaminMutu":isi.penjaminmutu,
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.post(urlperizinan+'perizinan/ajukanizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.verifikasiizin]({},isi){
        //data isi {id:0,penjaminmutu:"",izindeputi:"",dokumen:filedokumen}

        let data ={
            "penjaminMutu":isi.penjaminMutu,
            "izinDeputi": isi.izinDeputi,
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/verfizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)

            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizin]({},id){

        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/otorisasi/'+id,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)

                }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizinlist]({},isi){

        //isi data "0,0,0"
        return new Promise((resolve,reject)=>{

            Axios.put(urlperizinan+'perizinan/otorisasi?listid='+isi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);

            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api izin

    // download dokumen surat deputi
    [Action.izin.downloaddokumenSD]({},isi){

        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/1/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat deputi

    // download dokumen surat pengajuan
    [Action.izin.downloaddokumenSP]({},isi){

        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/2/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat pengajuan

    // edit verifikasidiklat
    [Action.diklat.verifikasiStatus]({},isi){

        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/verifikasi/'+isi.id+'/'+isi.verifikasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)

            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end verifikasidiklat

    // otorisasi status
    [Action.diklat.otorisasiStatus]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/otorisasi/'+isi.id+'/'+isi.otorisasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end otorisasi status
    //api user
    [Action.user.tambahuser]({},isi){
        return new Promise((resolve,reject)=>{
            let data={
                "username":isi.username,
                "password":isi.password,
                "kementrianLembaga":isi.kld,
                "nama":isi.nama,
                "lemdik":isi.lemdik,
                "email":"email@gmail.com"
            };
            Axios.post(urluser+'user/simpanuser/'+isi.role,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userbyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilnamauser]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/ambilusernama?nama='+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilsemuauser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userpenandatangan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftarpenandatangan?kld='+isi.kld+'&lemdik='+isi.lemdik,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatpeserta]({},isi){
        //isi data {nip:"",instansi:""}
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpanpeserta/'+isi.nip+"/"+isi.instansi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatuser](){
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpankld','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambiluserbypembuat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftaruser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantiprofil]({},isi){
        //isi data {id:0,email:"",username:"",idinstansi:""}
        let data={
            "email":isi.email,
            "username":isi.username,
            "idInstansi":isi.idinstansi
        }
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantiprofil/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantipass]({},isi){
        //isi data {id:0,password:"",confirm:""}
        var formdata = new FormData()
        formdata.append('password',isi.password)
        formdata.append('confirm',isi.confirm)
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantipass/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.hapususer]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urluser+'user/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api user

    //api sertifikat
     [Action.tambahdiklat]({commit,state},diklat){
        return new Promise ((resolve,reject)=>{

            let form ={
                "instansiPembina" : diklat.instansiPembina,
                "lembagaDiklat" : diklat.lembagaDiklat,
                "namaProgramDiklat" : diklat.namaProgramDiklat,
                "jenisProgramDiklat" : diklat.jenisProgramDiklat,
                "tahun" : diklat.tahun,
                "tempat": diklat.tempat,
                "angkatan" : diklat.angkatan,
                "penyelenggara" : diklat.penyelenggara,
                "tanggalPelaksanaan" : diklat.tanggalPelaksanaan,
                "tanggalSelesai" : diklat.tanggalSelesai,
                "kompetensi" : diklat.kompetensi,
                "persyaratanPeserta" : diklat.persyaratanPeserta,
                "saranaDanPrasarana" : diklat.saranaDanPrasarana,
                "evaluasiPeserta" : diklat.evaluasiPeserta,
                "evaluasiWidyaiswara" : diklat.evaluasiWidyaiswara,
                "evaluasiPenyelenggaraan" : diklat.evaluasiPenyelenggaraan,
                "sertifikasi" : diklat.sertifikasi
            }

            var formdiklat = new FormData()
            formdiklat.append('diklatJson',JSON.stringify(form))
            Axios.post('http://intranet.lan.go.id:8555/diklat/save',formdiklat,
            {
                headers:{
                    "Authorization":'Bearer '+window.$nuxt.$cookies.get('token'),
                    "Content-Type":'application/x-www-form-urlencoded'
                },
            }
            )
            .then(res=>{
                this.$router.push('/dashboard/diklat')
            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },

    [Action.databkn]({},data){
        return new Promise((resolve,reject)=>{
            Axios.get('http://sirela.lan.go.id:8080/bkn/datapegawai/'+data.nip,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }) .then(res=>{resolve(res)}
            ).catch(err=>{reject(err)})
        })
    },

    [Action.daftardiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/lemdik',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.semuadiklat]({state,commit}){
        return new Promise((resolve,reject)=>{
            Axios.get('http://localhost:8091/diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    // [Action.semuadiklat]({state,commit},status){
    //     return new Promise((resolve,reject)=>{
    //         Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
    //             headers:{
    //                  'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
    //             }
    //         }).then(res=>{resolve(res)}
    //         ).catch(err=>{reject(err)}
    //         )
    //     })
    // },

    [Action.lihatstatusdiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ubahstatusdiklat]({state,commit},isi){
        var formdiklat = new FormData()

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/updatestatus/'+isi.diklat+'/'+isi.status,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                }
            }).then(res=>{
                resolve(res)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            ).catch(err=>{reject(err)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            )
        })
    },

    [Action.uploadpeserta]({state,commit},diklat){
        var formdiklat = new FormData()
        formdiklat.append('fileSurat',diklat.file)

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/datapeserta/'+diklat.diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                },
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.datapeserta]({state,commit},id){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/datapeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdlemdik]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatanganlemdik/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttddeputi]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangandeputi/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdkepala]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangankepala/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end sertifikat

    //api sipka
   [Action.sipka.sgolpang](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'MGolPang',).then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },

   [Action.sipka.sprovinsi](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'Mprovinsi').then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },
   [Action.sipka.skabupaten](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'Mkabupaten').then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },
   [Action.sipka.slemdik](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'Mlemdik').then(res=>{resolve(res)
           }
           ).catch(err=>{reject(err)}
           )
       })
   },
   [Action.sipka.instansi](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'Minstansi').then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },
   [Action.sipka.prodik](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'MProgramDiklat').then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },
   [Action.sipka.akreditasi](){
       return new Promise((resolve,reject)=>{
           Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
           ).catch(err=>{reject(err)}
           )
       })
   },
   //end api sipka


    //api master
    [Action.master.golpang](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MGolPang').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.provinsi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mprovinsi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.kabupaten](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mkabupaten').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.lemdik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mlemdik').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.Minstansi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Minstansi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.prodik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MProgramDiklat').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.akreditasi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    //end api master

    //web socket
    [Action.send]() {
        if (this.stompClient && this.stompClient.connected) {
        //   //(send_message);
          this.stompClient.send("/app/diklat", "update berhasil", {});
        }
      },
      [Action.connect]({commit,state}) {
        //commit(`${[Mutation.connected]}`,true);
        this.socket = new SockJS(urldiklat+"gs-guide-websocket");
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect(
          {},
          frame => {
            commit(`${[Mutation.connected]}`,true);
            this.stompClient.subscribe("/front/alldiklat", tick => {
              commit(`${[Mutation.received_messages]}`,JSON.parse(tick.body))

              //this.received_messages.push(JSON.parse(tick.body).konten);
            });
          },
          error => {
            commit(`${[Mutation.connected]}`,false);
          }
        );
      },
      [Action.disconnect]({commit}) {
        if (this.stompClient) {
          this.stompClient.disconnect();
        }
        commit(`${[Mutation.connected]}`,false);
      },
      [Action.tickleconn]() {
        this.$store.state.api.connected? this.disconnect() : this.connect();
      }
    //end  web socket




}
