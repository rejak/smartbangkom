export const Action = {
    // api
    tambahdiklat:'tambahdiklat',
    semuadiklat:'semuadiklat',
    uploadpeserta:'uploadpeserta',
    datapeserta:'datapeserta',
    ubahstatusdiklat:'ubahstatusdiklat',
    lihatstatusdiklat:'lihatstatusdikat',
    ttdlemdik:'ttdlemdik',
    ttddeputi:'ttddeputi',
    ttdkepala:'ttdkepala',
    daftardiklat:'daftardiklat',
    //end api

    //api diklat
    diklat:{
        ubahtanggalttd:'ubahtanggalttd',
        ambildiklatbyuserid:'ambildiklatbyuserid',
        penandatangan:'penandatangan',
        downloaddoc:'downloaddoc',
        ambilsemuadiklat:'ambilsemuadiklat',
        ambildiklatbyuser:'ambildiklatbyuser',
        simpandiklat:'simpandiklat',
        kirimdiklat:'kirimdiklat',
        hitungpeserta:'hitungpeserta',
        //detail
        ambildetaildiklat:'ambildetaildiklat',
        simpandetail:'simpandetail',
        editdetail:'editdetail',
        deletedetail:'deletedetail',
        //enddetail
        ambilangkatan:'ambilangkatan',
        simpanangkatan:'simpanangkatan',
        editangkatan:'editangkatan',
        delelteangkatan:'delelteangkatan',
        // verifikasistatus
        verifikasiStatus:'verifikasiStatus',
        // end verifikasi status
        // otorisasi
        otorisasiStatus:'otorisasiStatus',
        uploadkra:'uploadkra'
        // end otorisasi
        //angkatan
        //endangkatan
    },
    //end api diklat

    //api kra
    kra:{
        tambah:'tambah',
        edit:'edit',
        ambil:'ambil',
        hapus:'hapus'
    },
    //end api kra
    //api cert
    cert:{
        cekganti:'cekganti',
        generate:'generate',
        ttdcert:'ttdcert',
        tandacert:'tandacert',
        ttdcertkalan:'ttdcertkalan',
        tandakalan:'tandakalan',
        ttdcertdeputi:'ttdcertdeputi',
        tandadeputi:'tandadeputi',
        gbtttd:'gbrttd',
        datagbr:'datagbr'
    },
    //end api cert
    //api peserta
    peserta:{

        ambilpesertabyid:'ambilpesertabyid',
        tambahkra:'tambahkra',
        tambahnilai:'tambahnilai',
        updatePeserta:'updatePeserta',
        ambilbkn:'ambilbkn',
        simpanpesertakelompok:'simpanpesertakelompok',
        ambilpesertabydiklat:'ambilpesertabydiklat',
        ambilpesertabyangkatan:'ambilpesertabyangkatan',
        simpanpeserta:'simpanpeserta',
        editpeserta:'editpeserta',
        editfotopeserta:'editfotopeserta',
        deletepeserta:'deletepeserta',
        pindahangkatan:'pindahangkatan',
        pindahdiklat:'pindahdiklat',
        downloadsampel:'downloadsampel',
    },
    //end api peserta

    //api izin
    izin:{
        ambilizin:'ambilizin',
        ambilizinbyuser:'ambilizinbyuser',
        editizin:'editizin',
        deleteizin:'deleteizin',
        ajukanizin:'ajukanizin',
        verifikasiizin:'verifikasiizin',
        otorisasiizin:'otorisasiizin',
        otorisasiizinlist:'otorisasiizinlist',
        downloaddokumenSD:'downloaddokumenSD', //surat deputi
        downloaddokumenSP:'downloaddokumenSP' //surat pengajuan
    },
    //end izin

    //api user
    user:{
        userpenandatangan:'userpenandatangan',
        userbyid:'userbyid',
        tambahuser:'tambahuser',
        ambilsemuauser:'ambilsemuauser',
        buatuser:'buatuser',
        ambiluserbypembuat:'ambiluserbypembuat',
        buatpeserta:'buatpeserta',
        gantiprofil:'gantiprofil',
        gantipass:'gantipass',
        hapususer:'hapususer',
        ambilnamauser:'ambilnamauser',
    },
    //end user

    //api bkn
    databkn:'databkn',
    //api bkn

    //sipka
    sipka:{
        provinsi:'provinsi',
        kabupaten:'kabupaten',
        lemdik:'lemdik',
        prodik:'prodik',
        instansi:'instansi',
        akreditasi:'akreditasi',
        golpang:'golpang1',
    },
    
    //master
    master:{
        provinsi:'provinsi',
        kabupaten:'kabupaten',
        lemdik:'lemdik',
        prodik:'prodik',
        instansi:'instansi',
        akreditasi:'akreditasi',
        golpang:'golpang',
    },
    //end master

    //websocket
    send:'send',
    connect:'connect',
    disconnect:'disconnect',
    tickleconn:'tickleconn',
    //endwebsocket

};

export const Mutation ={
    diklat:'diklat',
    received_messages: 'received_messages',
    send_message: 'send_message',
    connected: 'connected',
    diklattsk:'diklattsk',
    error:'error',
}
