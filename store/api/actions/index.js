import {Action,Mutation} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';


import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

var urldiklat="https://smartbangkom.lan.go.id:8091/";
var urlpeserta="https://smartbangkom.lan.go.id:8092/";
var urlperizinan="https://smartbangkom.lan.go.id:8093/";
var urluser ="https://smartbangkom.lan.go.id:8090/";
var urlmaster="https://sipka.lan.go.id/api/";
var urlDev ="https://smartbangkom.lan.go.id:8094/";
var urlcert = "https://smartbangkom.lan.go.id:8096/";
var urlkra = "https://smartbangkom.lan.go.id:8095/";

export default {

    //api diklat
    //upload
    [Action.diklat.uploadkra]({},isi){

        var formdata = new FormData()
        formdata.append('permintaankra',isi.permintaan)
        formdata.append('daftarhadir',isi.daftarhadir)

        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/uploaddokumen/'+isi.iddiklat,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    //end upload
    [Action.diklat.downloaddoc]({},nama){

        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/downloaddockra/'+nama+'.',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.penandatangan]({},isi){
        return new Promise((resolve,reject)=>{

            Axios.put(urldiklat+'diklat/penandatangan/'+isi.iddiklat+"/"+isi.ttd1+"/"+isi.ttd2+"/"+isi.ttd3,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilsemuadiklat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilbursa](dispatch,isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/ambilbursa/'+isi.page+'/'+isi.limit+'/'+isi.status+'?judul',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.cariBursa](dispatch,isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('judul',isi.judul)
            Axios.get(urldiklat+'diklat/caribursabyjudul/'+isi.page+'/'+isi.limit+'/'+isi.status+'?judul='+isi.judul,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.getPengajuan](dispatch,isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/getpengajuan/'+isi.page+'/'+isi.limit+'/'+isi.jenis+'/'+isi.status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.cariPengajuan](dispatch,isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('judul',isi.judul)
            Axios.get(urldiklat+'diklat/caribyjudul/'+isi.page+'/'+isi.limit+'/'+isi.jenis+'/'+isi.status+'?judul='+isi.judul,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuserid]({},iduser){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byiduser/'+iduser,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.getDaftarTtd]({},iduser){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/getdaftarttd/'+iduser,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbylemdikid]({},iduser){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byidlemdik/'+iduser,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpandiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/simpan',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.kirimdiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/kirim',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.hitungpeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/countpeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildetaildiklat]({},id){
        //(id);

        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res);} //('sini');}
            ).catch(err=>{reject(err);} //('sono');}
            )
        })
    },
    [Action.diklat.simpandetail]({},isi){
        //data isi {jumpes:0,asal:"",sumber:"",pola:""}
        return new Promise((resolve,reject)=>{
            let data={
                "jumpes":isi.jumpes,
                "asalInstansi":isi.asal,
                "sumberAnggaran":isi.sumber,
                "polaPenyelenggaraan":isi.pola
            };
            Axios.post(urldiklat+'diklat/detail/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ubahtanggalttd]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            // Axios.put(urldiklat+'diklat/tglttd/'+isi.iddiklat+'/'+isi.tgl+'/'+isi.tipe,'', {headers:headers})
            Axios.put(urldiklat+'diklat/tglttd/'+isi.iddiklat+'/'+isi.tgl,'', {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
        })
    },
    [Action.diklat.editdetail]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up = isi[n];
                Axios.put(urldiklat+'diklat/editDetail/'+element_up.id, element_up, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        })
    },
    [Action.diklat.deletedetail]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.deletediklat]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilangkatan]({},id){
        //(id);

        return new Promise((resolve,reject)=>{
            Axios.get('https://smartbangkom.lan.go.id:8091/diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{
                resolve(res)
                //(res);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpanangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let data={
                "alamat":isi.alamat,
	            "daerah":isi.daerah
            };
            Axios.post(urldiklat+'diklat/angkatan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.editangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up_angkatan = isi[n];
                Axios.put(urldiklat+'diklat/angkatan/'+element_up_angkatan.id, element_up_angkatan, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        })
    },
    [Action.diklat.delelteangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.postTemplateTeknis]({},isi){
        var formdata = new FormData()
        formdata.append('formatcert',isi.file[0])
        return new Promise((resolve, reject)=>{
            Axios.put(urldiklat+"diklat/uploadformat/"+isi.idDiklat,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.diklat.putValidasi]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/validasiatasan/'+isi.id+'/'+isi.value,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end diklat

    //api kra
    [Action.cert.datagbr]({},namagbr){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/gambarttd/'+namagbr+'.','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.gbtttd]({},isi){
        var formdata = new FormData()
        formdata.append('gambar',isi.base64)
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/simpangbrttd/'+isi.iduser,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.cekFile]({},isi){
        var formdata = new FormData()
        formdata.append('dokumen', isi)
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/cekfile/',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.ttdcert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttddigital/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(function (error) {
                reject(error)
                if (error.response) {
                // commit(`${[Mutation.error]}`,error.response.status)
                //(error.response.data);
                //(error.response.status);
                //(error.response.headers);
                }
            }

            )
        })
    },
    [Action.cert.ttdcertkalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttdkalandev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.ttdcertdeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttddeputidev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    //riil
    [Action.cert.cekganti]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ubahpdf/'+isi.idpeserta,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandacert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttdlemdik/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandakalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttdkalan/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.cert.tandadeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/ttddeputi/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //endriil
    [Action.cert.generate]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlcert+'digitalsignature/gensert/'+isi.id,{
                "jp":isi.jp,
                "penyelenggara":isi.p,
                "status":isi.sts,
                "pm":isi.pm,
                "pminstansi":isi.pminstansi,
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.kra.ambil](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'kra',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.kra.edit]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'kra/'+isi.id,{
                "jenisprogram":isi.jenis,
                "nomor":isi.nomor,
                "tahun":isi.tahun
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    
    //end api kra

    //api peserta
    [Action.peserta.tambahkra]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahkra/'+isi.idpeserta+'/'+isi.idkra,{
                "nomorKra":isi.kra
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.jumlahByTahun]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/jumlahsemuabytahun/'+isi.tahun+'/'+isi.isiKra+'/'+isi.id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.jumlahByIdProgram]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/jumlahbyidprogram/'+isi.id+'/'+isi.isiKra+'/'+isi.status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.jumlahByIdProgramEsttp]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/jumlahbyidprogram/'+isi.id+'/'+isi.isiKra+'/'+isi.sttp+'/'+isi.status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.jumlahByGender]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/jumlahbygender/'+isi.jk+'/'+isi.status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.tunda.sertif]({},id){
        console.log(id);
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/updatetunda/'+ id,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.tambahnilai]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahnilai/'+isi.idpeserta,{
                "proyekPerubahan":isi.proyek,
                "nilai":isi.nilai
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.peserta.updatePeserta]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/updatekuota/'+isi.iddiklat+'/'+isi.kuota,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyrole]({},role){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftarbyrole/'+role,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilbkn]({},nip){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'bkn/datapegawai/'+nip,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpesertakelompok]({},isi){
        //data isi {instansi:"",alamat:"",dokumen:"",jeniskerja:"",id:0}
        let form={
            "asalInstansi":isi.instansi,
            "alamatKantor":isi.alamat,
            "jenisPekerjaan":isi.jeniskerja,
        }
        var formdata = new FormData()
        formdata.append('excel',isi.dokumen)
        formdata.append('isi',JSON.stringify(form))

        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelompok/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabydiklat]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyIdPeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/userPeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertatanpaid]({}){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.revisiSttp]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/revisiSttp/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{
            let data={
                    "jabatan":isi.jabatan,
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol
            };
            Axios.post(urlpeserta+'peserta',data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.editpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{
            //(isi);

            let data={
                    "angkatan":isi.angakatan,
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol,
                    "jabatan":isi.jabatan
            };
            Axios.put(urlpeserta+'peserta/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.editfotopeserta]({},isi){
        //isi data {id:0,foto:filefoto}
        //(isi);
        return new Promise((resolve,reject)=>{
            let formdata = new FormData()
		    formdata.append('foto', isi.foto)
            Axios.put(urlpeserta+'peserta/editfoto/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.getfotopeserta]({},foto){
        //isi data {id:0,foto:filefoto}
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/getfoto/'+foto+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.deletepeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahangkatan]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelaspeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahdiklat]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/pindahpeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.downloadsampel]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/download/'+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api peserta

    //api izin
    [Action.izin.ambilizin](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ambilizinbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/izinbyuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.editizin]({},isi){
        //data isi {id:0,iddiklat:0,penjaminmutu:"",izindeputi:""}
        return new Promise((resolve,reject)=>{
            let data={
                "idDiklat": isi.iddiklat,
                "penjaminMutu": isi.penjaminmutu,
                "izinDeputi": isi.izindeputi,
            }
            Axios.put(urlperizinan+'perizinan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.editizinUpload]({},isi){
        //data isi {id:0,iddiklat:0,penjaminmutu:"",izindeputi:""}
        return new Promise((resolve,reject)=>{
            let data={
                "idDiklat": isi.iddiklat,
                "penjaminMutu": isi.penjaminmutu,
            }
            var formdata = new FormData()
            formdata.append('dokumen',isi.dokumen)
            formdata.append('isi',JSON.stringify(data))
            Axios.put(urlperizinan+'perizinan/revisiizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.deleteizin]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlperizinan+'perizinan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ajukanizin]({},isi){
        //(isi.penjaminmutu);

        //data isi {id:0,penjaminmutu:"",dokumen:filedokumen}
        let data ={
            "penjaminMutu":isi.penjaminmutu,
            "idDiklat": isi.id
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.post(urlperizinan+'perizinan/ajukanizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.verifikasiizin]({},isi){
        //data isi {id:0,penjaminmutu:"",izindeputi:"",dokumen:filedokumen}
        //(isi);

        let data ={
            "penjaminMutu":isi.penjaminMutu,
            "izinDeputi": isi.izinDeputi,
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/verfizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)

                //('tes sukses');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizin]({},id){
        //(id);

        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/otorisasi/'+id,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                //('berhasil otorisasi izin');

                }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizinlist]({},isi){
        //(isi);

        //isi data "0,0,0"
        return new Promise((resolve,reject)=>{

            Axios.put(urlperizinan+'perizinan/otorisasi?listid='+isi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                //('berhasil');

            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api izin

    // download dokumen surat deputi
    [Action.izin.downloaddokumenSD]({},isi){
        //(isi);

        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/1/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat deputi

    // download dokumen surat pengajuan
    [Action.izin.downloaddokumenSP]({},isi){
        //(isi);

        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/2/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat pengajuan

    // edit verifikasidiklat
    [Action.diklat.verifikasiStatus]({},isi){
        //(isi);

        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/verifikasi/'+isi.id+'/'+isi.verifikasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)

                //('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end verifikasidiklat

    // otorisasi status
    [Action.diklat.otorisasiStatus]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/otorisasi/'+isi.id+'/'+isi.otorisasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                //('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end otorisasi status
    //api user
    [Action.user.tambahuser]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            let data={
                "username":isi.username,
                "password":isi.password,
                "kementrianLembaga":isi.kld,
                "idInstansi":isi.idInstansi,
                "nama":isi.nama,
                "lemdik":isi.lemdik,
                "email":"email@gmail.com"
            };
            Axios.post(urluser+'user/simpanuser/'+isi.role,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                //('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userbyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilnamauser]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/ambilusernama?nama='+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilsemuauser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userpenandatangan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftarpenandatangan?kld='+isi.kld+'&lemdik='+isi.lemdik,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatpeserta]({},isi){
        //isi data {nip:"",instansi:""}
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpanpeserta/'+isi.nip+"/"+isi.instansi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatuser](){
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpankld','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambiluserbypembuat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftaruser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantiprofil]({},isi){
        //isi data {id:0,email:"",username:"",idinstansi:""}
        let data={
            "email":isi.email,
            "username":isi.username,
            "idInstansi":isi.idinstansi,
            "nama":isi.nama,
            "kementrianLembaga":isi.kementrianLembaga,
            "nik":isi.nik,
            "lemdik":isi.lemdik,
            "akronim":isi.akronim,
        }
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantiprofil/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantipass]({},isi){
        //isi data {id:0,password:"",confirm:""}
        var formdata = new FormData()
        formdata.append('password',isi.password)
        formdata.append('confirm',isi.confirm)
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantipass/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.hapususer]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urluser+'user/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api user

    //api sertifikat
     [Action.tambahdiklat]({commit,state},diklat){
        return new Promise ((resolve,reject)=>{

            let form ={
                "instansiPembina" : diklat.instansiPembina,
                "lembagaDiklat" : diklat.lembagaDiklat,
                "namaProgramDiklat" : diklat.namaProgramDiklat,
                "jenisProgramDiklat" : diklat.jenisProgramDiklat,
                "tahun" : diklat.tahun,
                "tempat": diklat.tempat,
                "angkatan" : diklat.angkatan,
                "penyelenggara" : diklat.penyelenggara,
                "tanggalPelaksanaan" : diklat.tanggalPelaksanaan,
                "tanggalSelesai" : diklat.tanggalSelesai,
                "kompetensi" : diklat.kompetensi,
                "persyaratanPeserta" : diklat.persyaratanPeserta,
                "saranaDanPrasarana" : diklat.saranaDanPrasarana,
                "evaluasiPeserta" : diklat.evaluasiPeserta,
                "evaluasiWidyaiswara" : diklat.evaluasiWidyaiswara,
                "evaluasiPenyelenggaraan" : diklat.evaluasiPenyelenggaraan,
                "sertifikasi" : diklat.sertifikasi
            }

            var formdiklat = new FormData()
            formdiklat.append('diklatJson',JSON.stringify(form))
            Axios.post('http://intranet.lan.go.id:8555/diklat/save',formdiklat,
            {
                headers:{
                    "Authorization":'Bearer '+window.$nuxt.$cookies.get('token'),
                    "Content-Type":'application/x-www-form-urlencoded'
                },
            }
            )
            .then(res=>{
                //(res)
                this.$router.push('/dashboard/diklat')
            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },

    [Action.databkn]({},data){
        return new Promise((resolve,reject)=>{
            Axios.get('http://sirela.lan.go.id:8080/bkn/datapegawai/'+data.nip,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }) .then(res=>{resolve(res)}
            ).catch(err=>{reject(err)})
        })
    },

    [Action.daftardiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/lemdik',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.semuadiklat]({state,commit}){
        return new Promise((resolve,reject)=>{
            Axios.get('http://localhost:8091/diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    // [Action.semuadiklat]({state,commit},status){
    //     return new Promise((resolve,reject)=>{
    //         Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
    //             headers:{
    //                  'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
    //             }
    //         }).then(res=>{resolve(res)}
    //         ).catch(err=>{reject(err)}
    //         )
    //     })
    // },

    [Action.lihatstatusdiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ubahstatusdiklat]({state,commit},isi){
        var formdiklat = new FormData()

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/updatestatus/'+isi.diklat+'/'+isi.status,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                }
            }).then(res=>{
                resolve(res)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            ).catch(err=>{reject(err)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            )
        })
    },

    [Action.uploadpeserta]({state,commit},diklat){
        //(window.$nuxt.$cookies.get('token'))
        //('isi: '+diklat.diklat)
        var formdiklat = new FormData()
        formdiklat.append('fileSurat',diklat.file)

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/datapeserta/'+diklat.diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                },
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.datapeserta]({state,commit},id){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/datapeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdlemdik]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatanganlemdik/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttddeputi]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangandeputi/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdkepala]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangankepala/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end sertifikat

    //api sipka
    [Action.sipka.golpang](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MGolPang',).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.sipka.provinsi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mprovinsi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.kabupaten](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mkabupaten').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.lemdik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mlemdik').then(res=>{resolve(res)
            //(res.data);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.instansi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Minstansi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.prodik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MProgramDiklat').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.akreditasi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api sipka

    //api master
    [Action.master.getUserByNamaLemdik]({},isi){
        //(isi)
        var formData = new FormData();
        formData.append('lemdik','re')
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/lemdikbyidSipka',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                //('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.golpangMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/golpang',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postgolpangMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/golpang',{
                    "idSipka": isi.idSipka,
                    "pangkat": isi.pangkat,
                    "gol": isi.gol,
                    "gol_pangkat_tempat": isi.gol_pangkat_tempat,
                    "gol_pangkat_tingkat": isi.gol_pangkat_tingkat
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putgolpangMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/golpang/'+isi.id,{
                    "idSipka": isi.idSipka,
                    "pangkat": isi.pangkat,
                    "gol": isi.gol,
                    "gol_pangkat_tempat": isi.gol_pangkat_tempat,
                    "gol_pangkat_tingkat": isi.gol_pangkat_tingkat
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.provinsiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/provinsi',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postprovinsiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/provinsi',{
                "idSipka": isi.idSipka,
                "namaProvinsi": isi.namaProvinsi,
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putprovinsiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/provinsi/'+isi.id,{
                "idSipka": isi.idSipka,
                "namaProvinsi": isi.namaProvinsi,
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.kabupatenMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/kabupaten',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postkabupatenMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/kabupaten',{
                "idSipka": isi.idSipka,
                "namaKabupaten": isi.namaKabupaten,
                "provinsi": isi.provinsi
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putkabupatenMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/kabupaten/'+isi.id,{
                "idSipka": isi.idSipka,
                "namaKabupaten": isi.namaKabupaten,
                "provinsi": isi.provinsi
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.lemdikMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/lemdik',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.addlemdikMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/lemdik',{
                "instansi": isi.instansi,
                "namaLemdik": isi.namaLemdik,
                "provinsi":isi.provinsi,
                "idSipka": isi.idSipka
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.updatelemdikMaster]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/lemdik/'+isi.id,{
                "instansi": isi.instansi,
                "namaLemdik": isi.namaLemdik,
                "provinsi":isi.provinsi,
                "idSipka": isi.idSipka
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postinstansiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/instansi',
            {
                "idSipka": isi.idSipka,
                "namaInstansi": isi.namaInstansi,
                "kode_bkn": isi.kode_bkn,
            }
            ,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putinstansiMaster]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/instansi/'+isi.id,
            {
                "idSipka": isi.idSipka,
                "namaInstansi": isi.namaInstansi,
                "kode_bkn": isi.kode_bkn,
            }
            ,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.instansiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/instansi',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.prodikMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/prodik',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.addprodikMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/prodik',{
                "kuotaDiklat": isi.kuotaDiklat,
                "namaDiklat": isi.namaDiklat,
                "idSipka": isi.idSipka,
                "sektor": isi.sektor
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.updateprodikMaster]({},isi){
        //(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/prodik/'+isi.id,{
                "kuotaDiklat": isi.kuotaDiklat,
                "namaDiklat": isi.namaDiklat,
                "idSipka": isi.idSipka,
                "sektor": isi.sektor
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.akreditasiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // Template
    [Action.master.getTemplate](){
        return new Promise((resolve, reject)=>{
            Axios.get(urlDev+"master/template",{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.postTemplate]({},isi){
        //(isi);
        let data ={
            "namaTemplate":isi.namaTemplate,
            "jenisPelatihan": isi.jenisPelatihan,
            "instansi": isi.instansi,
            "penjaminmutu": isi.penjaminmutu,
            "totaljp": isi.totaljp
        }
        var formdata = new FormData()
        formdata.append('formatcert',isi.file[0])
        formdata.append('template',JSON.stringify(data))
        return new Promise((resolve, reject)=>{
            Axios.post(urlDev+"master/template",formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
                //(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.putTemplate]({},isi){
        //(isi.file);
        //(isi.file[0]);
        let data ={
            "namaTemplate":isi.namaTemplate,
            "jenisPelatihan": isi.jenisPelatihan,
            "instansi": isi.instansi,
            "penjaminmutu": isi.penjaminmutu,
            "totaljp": isi.totaljp
        }
        var formdata = new FormData()
        if(isi.file.length != 0){
            formdata.append('formatcert',isi.file[0])
        }
        formdata.append('template',JSON.stringify(data))
        return new Promise((resolve, reject)=>{
            Axios.put(urlDev+"master/template/"+isi.id,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.deleteTemplate]({},id){
        return new Promise((resolve, reject)=>{
            Axios.delete(urlDev+"master/template/"+id,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    // end template
    //end api master

    //web socket
    [Action.send]() {
        if (this.stompClient && this.stompClient.connected) {
        //   //(send_message);
          this.stompClient.send("/app/diklat", "update berhasil", {});
        }
      },
      [Action.connect]({commit,state}) {
        //commit(`${[Mutation.connected]}`,true);
        //('koneksi '+state.connected);
        this.socket = new SockJS(urldiklat+"gs-guide-websocket");
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect(
          {},
          frame => {
            commit(`${[Mutation.connected]}`,true);
            //('koneksi '+state.connected);
            //(frame);
            this.stompClient.subscribe("/front/alldiklat", tick => {
              //(tick);
              //(JSON.parse(tick.body));
              commit(`${[Mutation.received_messages]}`,JSON.parse(tick.body))

              //this.received_messages.push(JSON.parse(tick.body).konten);
            });
          },
          error => {
            //(error);
            commit(`${[Mutation.connected]}`,false);
          }
        );
      },
      [Action.disconnect]({commit}) {
        if (this.stompClient) {
          this.stompClient.disconnect();
        }
        commit(`${[Mutation.connected]}`,false);
      },
      [Action.tickleconn]() {
        this.$store.state.api.connected? this.disconnect() : this.connect();
      },
    //end  web socket


}
