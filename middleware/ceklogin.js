

export default  (context)=>{
    // //('cek login')

    // //('tes '+ context.app.context.app.$cookies.get('token'))

    if(context.app.context.app.$cookies.get('isijwt').authorities[0]=="ROLE_LEMDIK"){
        context.app.context.app.$cookies.set('status',5)
    }
    if(context.app.context.app.$cookies.get('isijwt').authorities[0]=="ROLE_ADMIN"){
        context.app.context.app.$cookies.set('status',1)
    }
    if(context.app.context.app.$cookies.get('isijwt').authorities[0]=="ROLE_KEPALA_LEMDIK"){
        context.app.context.app.$cookies.set('status',2)
    }
    if(context.app.context.app.$cookies.get('isijwt').authorities[0]=="ROLE_DEPUTI"){
        context.app.context.app.$cookies.set('status',3)
    }
    if(context.app.context.app.$cookies.get('isijwt').authorities[0]=="ROLE_KEPALA"){
        context.app.context.app.$cookies.set('status',4)
    }
    
    var token=context.app.context.app.$cookies.get('token')
    
    if(!context.app.context.app.$cookies.get('token')){
       return context.redirect('/login')
    }
}